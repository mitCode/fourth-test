import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { List } from '../pages/list/list';
import { AddItem } from '../pages/list/add-item/add-item'
import { ItemEntry } from '../pages/list/item-entry/item-entry'
import { ItemService } from "../item-data/item.service";

@NgModule({
  declarations: [
    MyApp,
    ItemDetailsPage,
    List,
    AddItem,
    ItemEntry
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ItemDetailsPage,
    List,
    AddItem,
    ItemEntry
  ],
  providers: [
    ItemService,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
