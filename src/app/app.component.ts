import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import { List } from '../pages/list/list';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage = List;

  constructor() {
  }
}
