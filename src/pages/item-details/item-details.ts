import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { Item } from '../../item-data'

@Component({
  selector: 'page-item-details',
  templateUrl: 'item-details.html'
})
export class ItemDetailsPage {
  selectedItem: Item;

  constructor(public navParams: NavParams) {
    this.selectedItem = navParams.get('item');
  }
}
