import {TestBed, ComponentFixture, async, fakeAsync} from '@angular/core/testing';
import {IonicModule} from 'ionic-angular';
import {NavController} from 'ionic-angular';

import {ItemEntry} from '../../list/item-entry/item-entry';
import {ItemService} from "../../../item-data/item.service";
import {By} from '@angular/platform-browser';

class ItemServiceMock {
  constructor() {
  }

  deleteItem() {
  }
}


let comp: ItemEntry;
let fixture: ComponentFixture<ItemEntrys>;


describe('Component: Item Entry Component', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemEntry],
      providers: [{provide: ItemService, useClass: ItemServiceMock}, NavController],
      imports: [
        IonicModule.forRoot(ItemEntry)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    spyOn(ItemServiceMock.prototype, 'deleteItem').and.callThrough();

    fixture = TestBed.createComponent(ItemEntry);
    comp = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
    comp = null;
  });

  it('is created', () => {
    expect(fixture).toBeTruthy();
    expect(comp).toBeTruthy();
  });

  it('should call the item service and delete the item', fakeAsync(() => {
    let entry = fixture.debugElement.query(By.css('ion-icon'));
    entry.triggerEventHandler('click', new MouseEvent('click'));
    expect(ItemServiceMock.prototype.deleteItem).toHaveBeenCalledTimes(1);
  }));

});
