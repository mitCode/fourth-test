import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ItemDetailsPage } from '../../item-details/item-details';
import { Item } from '../../../item-data';
import { ItemService } from '../../../item-data/item.service';

@Component({
  selector: 'item-entry',
  templateUrl: 'item-entry.html'
})
export class ItemEntry {
  @Input() item: Item;

  constructor(
    private _navCtrl: NavController,
    private _itemService: ItemService
  ) {
  }

  openItem(): void {
    this._navCtrl.push(ItemDetailsPage, {
      item: this.item
    });
  }

  deleteItem(event: MouseEvent): void {
    event.stopPropagation();
    this._itemService.deleteItem(this.item);
  }
}
