import { Component } from '@angular/core';
import { Item } from '../../item-data';
import { ItemService } from '../../item-data/item.service';

@Component({
  selector: 'list',
  templateUrl: 'list.html'
})
export class List {
  public items: Item[];

  constructor(private _itemService: ItemService) {
    this._itemService.getItems().subscribe(items => this.items = items);
  }
}
