import {TestBed, ComponentFixture, async} from '@angular/core/testing';
import {IonicModule} from 'ionic-angular';
// import { MyApp } from './
import {Item} from '../../item-data';
import {List} from '../list/list';
import {AddItem} from '../list/add-item/add-item';
import {ItemEntry} from '../list/item-entry/item-entry';
import {ItemService} from "../../item-data/item.service";
import {Observable} from "rxjs";

const mockItems: Item[] = [{
  _id: {
    $oid: '1'
  },
  title: 'Lorem',
  description: 'Ipsum'
}, {
  _id: {
    $oid: '2'
  },
  title: 'title',
  description: 'description'
}];

class ItemServiceMock {
  constructor() {
  }

  getItems() {
    return Observable.of(mockItems)
  }
}


let comp: List;
let fixture: ComponentFixture<List>;


describe('Component: List Component', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [List, AddItem, ItemEntry],
      providers: [{provide: ItemService, useClass: ItemServiceMock}],
      imports: [
        IonicModule.forRoot(List)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    spyOn(ItemServiceMock.prototype, 'getItems').and.callThrough();

    fixture = TestBed.createComponent(List);
    comp = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
    comp = null;
  });

  it('is created', () => {
    expect(fixture).toBeTruthy();
    expect(comp).toBeTruthy();
  });

  it('should call the item service and initialise the items', () => {
    expect(ItemServiceMock.prototype.getItems).toHaveBeenCalledTimes(1);
  });

  it('is initialised with the items', () => {
    expect(comp['items']).toBe(mockItems);
  });

});
