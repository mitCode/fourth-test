import { Component } from '@angular/core';
import { ItemService } from '../../../item-data/item.service';

@Component({
  selector: 'add-item',
  templateUrl: 'add-item.html'
})
export class AddItem {
  constructor(private _itemService: ItemService) {
  }

  addItem(f): void {
    this._itemService.addItem(f.value, () => f.reset());
  }
}
