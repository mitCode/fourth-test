export interface Item {
  title: string;
  description: string;
  _id: {
    $oid: string
  };
}
