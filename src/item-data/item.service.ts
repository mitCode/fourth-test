import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import "rxjs/add/operator/map";
import { Http, Response } from '@angular/http';
import { Item } from '../item-data';

@Injectable()
export class ItemService {

  private _url: string = 'https://api.mongolab.com/api/1/databases/fourth/collections/fourth'
  private _apiKey = 'cVMTmvuDX-Chn2qrcUTRcI7oKAHk7HqD';
  private _items$: BehaviorSubject<Item[]> = new BehaviorSubject([]);

  constructor(private _http: Http) {
    this._fetchItems();
  }

  private _fetchItems() {
    this._http.get(this._url + '?apiKey=' + this._apiKey)
      .map((response: Response) => response.json())
      .subscribe(items => this._items$.next(items));
  }

  getItems(fetch = false): Observable<Item[]> {
    fetch && this._fetchItems();
    return this._items$;
  }

  addItem(item, complete?: Function): void {
    this._http.post(this._url + '?apiKey=' + this._apiKey, item)
      .map((response: Response) => response.json())
      .subscribe(added => {
        const items = this._items$.getValue();
        items.push(added);
        this._items$.next(items);
        complete && complete();
      });
  }

  deleteItem(item: Item): void {
    this._http.delete(`${this._url}/${item._id.$oid}?apiKey=${this._apiKey}`)
      .map((response: Response) => response.json())
      .subscribe(deleted => {
        const items = this._items$.getValue();
        items.splice(items.findIndex(item => item._id.$oid === deleted._id.$oid), 1);
        this._items$.next(items);
      });
  }

}
