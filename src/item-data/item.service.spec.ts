import {TestBed, inject, async, tick, fakeAsync} from '@angular/core/testing';
import { ItemService } from './item.service';
import { BaseRequestOptions, Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

describe('Item Service', () => {
    const mockResponse = [{
      _id: {
        $oid: '1'
      },
      title: 'Lorem',
      description: 'Ipsum',
    }, {
      _id: {
        $oid: '2'
      },
      title: 'title',
      description: 'description'
    }];

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [
              ItemService,
              MockBackend,
              BaseRequestOptions,
              {
                provide: Http,
                useFactory: (backend, options) => new Http(backend, options),
                deps: [MockBackend, BaseRequestOptions]
              }
            ],
            imports: [
              HttpModule
            ]
        }).compileComponents();

    });

    it('should construct', async(inject([ItemService, MockBackend], (itemService, mockBackend) => {
      expect(itemService).toBeDefined();
    })));

    describe('getItems', () => {

      var itemService;
      var mockBackend;

      beforeEach(inject([ItemService, MockBackend], (iS: ItemService, mBE: MockBackend) => {
        itemService = iS;
        mockBackend = mBE;
      }));

      it('should return an observable an empty list of items', () => {
          const result = itemService.getItems();
          result.subscribe(res => {
            expect(res).toEqual([]);
          });
      });

      it('should return an observable with all the items', fakeAsync(() => {
          mockBackend.connections.subscribe(conn => {
            conn.mockRespond(new Response(new ResponseOptions({ body: JSON.stringify(mockResponse) })));
          });
          const result = itemService.getItems(true);
          tick();
          result.subscribe(res => {
            expect(res).toEqual(mockResponse);
          });
        }));
    });

  describe('addItem', () => {

    var itemService;
    var mockBackend;

    beforeEach(inject([ItemService, MockBackend], (iS: ItemService, mBE: MockBackend) => {
      itemService = iS;
      mockBackend = mBE;
    }));
    it('should add the items and update the items observable', fakeAsync(() => {
      const newItem = { title: 'new', description: 'item'};

      mockBackend.connections.subscribe(conn => {
        conn.mockRespond(new Response(new ResponseOptions({ body: JSON.stringify(
          conn.request.method ? newItem : mockResponse) })));
      });
      const result = itemService.getItems(true);
      itemService.addItem(newItem);
      tick();
      result.subscribe(res => {
        expect(res).toEqual([...mockResponse, newItem]);
      });
    }));
  });

  describe('deleteItem', () => {

    var itemService;
    var mockBackend;

    beforeEach(inject([ItemService, MockBackend], (iS: ItemService, mBE: MockBackend) => {
      itemService = iS;
      mockBackend = mBE;
    }));
    it('delete the item and update the items observable', fakeAsync(() => {
      mockBackend.connections.subscribe(conn => {
        conn.mockRespond(new Response(new ResponseOptions({ body: JSON.stringify(
          !conn.request.method ? mockResponse : mockResponse[1]) })));
      });
      const result = itemService.getItems(true);
      itemService.deleteItem(mockResponse[1]);
      tick();
      result.subscribe(res => {
        const rest = mockResponse.slice();
        rest.pop();
        expect(res).toEqual(rest);
      });
    }));
  });

});
